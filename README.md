# Citotron: Wikipedia Citation Tools

## General aims

Written for the WikiScience research project at the Universitat Oberta de Catalunya, Citotron analyses scholarly citations in Wikipedia.  In the longer term it may become a general toolbox for working with citations on Wikipedia.  Particularly interesting would be to use these functions for automatic citation correction and cohesion in a Wikipedia bot.

## Quickstart

Count the frequency of titles whose ISBN, pmid, pmc or arxiv id is given in small.csv. Sleep one second before requests but use eight concurrent threads. You normally want twice as many threads as the number of CPU cores on the machine. After installation you can do:

`./citotron -m count -i small.csv -j 8`

On a machine with four CPU cores it should take around five minutes to query one thousand titles and count them.

## Current operation

Citotron can do a couple of different things at the moment:

## Count

`-m types`

Count 

Count the frequency of references to single journal or book titles in a CSV file such as the one in the data directory.  Miscellaneous modes available for further data analysis.  Uses free APIs to resolve references.

## Install

Instructions are for Debian derivative operating systems.

1. Install dependencies:

    sudo apt install python3-pip python3-bs4 python3-joblib python3-requests python3-isbnlib python3-matplotlib
    
    pip3 install those dependencies missing from the repos - e.g. sudo pip3 install -U joblib isbnlib

2. Clone `citotron` repository:

    git clone https://gitlab.com/maxigas/citotron3.git

3. Enter the directory and run `citotron`:

    cd citotron3
    ./citotron3.py --help
    ./citotron3.py -m count -i small.csv

## Configuration

Citotron has command line arguments for end users and a settings file for developers.

### Command line arguments

Output of `./citotron.py -h`:


    usage: citotron.py [-h] [-m MODE] [-k KIND] [-i INPUTFILE] [-c COMPAREFILE]
                       [-o OUTPUTFILE] [-d DISCRIMINANT] [-s SLEEP] [-t TIMEOUT]
                       [-j JOBS] [-v]

    optional arguments:
      -h, --help            show this help message and exit
      -m MODE, --mode MODE  Action to perform on data. 'count' counts the
                            frequency of publication (book, journal, etc.) titles
                            in the dataset. 'resolve' only resolves identifiers to
                            titles (used for testing). 'types' counts the
                            frequency of the identifiers themselves in the dataset
                            (used for understanding the dataset). 'scisbn' counts
                            the ratio of academic vs. non-academic references.
                            'journaltitles writes a list of journal titles
                            excluding books (the latter is identified by ISBN.'
      -k KIND, --kind KIND  Kind of identifier to process. Default is 'all'.
                            Current choices are 'isbn', 'pmid', 'pmd', 'doi',
                            'arxiv'.
      -i INPUTFILE, --inputfile INPUTFILE
                            Input filename. Default is 'input.csv'.
      -c COMPAREFILE, --comparefile COMPAREFILE
                            Compare with filename. Default is 'other.csv'.
      -o OUTPUTFILE, --outputfile OUTPUTFILE
                            Output filename. Default is 'output.csv'.
      -d DISCRIMINANT, --discriminant DISCRIMINANT
                            Plotting parameter. Exclude results when frequency is
                            lower than this constant. Use in combination with
                            'count' mode. Default is 10.
      -s SLEEP, --sleep SLEEP
                            Sleep between requests. Default is 0.
      -t TIMEOUT, --timeout TIMEOUT
                            Time to wait for response from server. Default is 6.
      -j JOBS, --jobs JOBS  Number of parallel requests. Default is 1.
      -v, --verbose         Verbose mode. Default is True.


### Settings file

Typical users don’t have to edit this file.

Settings are stored in `settings.py`:

    # Citotron settings
    
    datadir = "data"
    logfile = "log.csv"
    academic_publishers = "scientific_publishers.lst"
    journal_abbrevs = "journal_abbrevs.json"
    partial_journal_abbrevs = "journal_partial_abbrevs.json"
    pmid_journal_abbrevs = "pmid_journal_abbrevs.json"
        

## Issues

Issues, bugs, comments are tracked in the Gitlab [issue tracker](https://gitlab.com/maxigas/citotron3/issues).

## Screenshot

Running citotron with eight resolver processes:

![./citotron.py -i small.csv -j 8 -m ’count’](screenshot1.png)

(Right click on the image and choose “Open image…” to enlarge.)







