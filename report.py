#!/usr/bin/python3
# Wikipedia citation tools
# report.py
# Performance report

from collections import Counter as counter
from functools import lru_cache
from pprint import pprint

from args import args
from utils import readlog, readdata, readoutput, percent
import settings, os

def failrate():
    '''Report failure rate'''
    rows = readlog()
    total, fails = len(rows), 0
    for row in rows:
        if not row['title']:
            fails += 1
    rate = str((fails / total)*100)[:4]+"%"
    print("-="*15, "< FAIL RATE REPORT >", "=-"*15)
    print("Total rows:", total)
    print("Missing titles:", fails)
    print("Failure ratio:",  rate)


def types():
    """Counts how many rows of each kind (isbn, doi, etc.) are in the input csv."""
    rows = readdata()
    types = []
    for row in rows:
        types.append(row['type'])
    pprint(counter(types).most_common())


def scisbn():
    """Returns statistics about which ISBNs are academic"""
    rows = readlog()
    rows = [ r for r in rows if r['type'] == 'isbn' ]
    print("All books resolved:", len(rows))
    # ----
    rows_with_publishers = [ r for r in rows if r['publisher'] is not None ]
    nr_rows_with_publishers = len(rows_with_publishers)
    print("Books with known publishers:", nr_rows_with_publishers)
    # ----
    rows_from_academic_publishers = [ r for r in rows_with_publishers if r['academic?'] is True ]
    nr_rows_from_academic_publishers = len(rows_from_academic_publishers)
    print("Books with academic publishers:", nr_rows_from_academic_publishers)
    # ----
    nr_rows_not_from_academic_publishers = nr_rows_with_publishers - nr_rows_from_academic_publishers
    print("Books with non-academic publishers:", nr_rows_not_from_academic_publishers)
    # ----
    print("Percent of books with academic publishers from all books with known publishers:", percent(nr_rows_from_academic_publishers, nr_rows_with_publishers))


def mostpop(n):
    '''Report <n> most popular titles.'''
    print("-="*12, " < REPORT:", n, "MOST POPULAR TITLES > ", "=-"*12)
    rows = readoutput()
    rows = [ r for r in rows if r[0] ]
    for x in range(0,n):
        try:
            print(rows[x])
        except IndexError:
            print("Output has less than", n, "titles.")
            break
