# Contributing

Patches and pull requests are welcome!

# Resolvers

It should be particularly easy to write new resolvers.  Resolver functions are called `<kind>[<n>]_to_title`, were `<kind>` is the type of identifier in lowercase and `[<n>]` is the priority of the resolver (zero indexed, but zeroes are omitted).  For example for transforming ISBNs to publication titles, `isbn_to_title` is tried first, then `isbn1_to_title`, `isbn2_to_title`, etc. until one succeeds.  A resolver function can take any measures necessary to resolve an identifier to a title.  It accepts an identifier and returns a string containing the title or `False` on failure.

# Terminology

Column names in the CSV file overlap with Python reserved words, so we rename them:

* type → `kind`
* id → `uid`

Internal terminology:

* `rows` is the internal representation of a CSV file;
* `serial` is the number of row in a rows (starts from 1);
* `uid` is an ISBN, arXiv, DOI, etc. that uniquely identifies a publication;
* `kind` is whether the uid is ISBN, arXiv, DOI, etc.

# Roadmap

 1. Normalisation of titles
 2. Deduplication of titles
 3. Comparison of CSVs
 4. Redundant resolvers for greater success rate (note)
 5. TBA…


