Design goals for version 3:

# TODO

 * restartable
 * no global variables ex. argv
 * no dynamic globals
 * check alan irwin

# DONE

 * separate progress bar functions
 * efficient parallelism: Parallel default backend was best (asyncio/aiohttp was not fast)
 * keeps data
 * one line at a time
 * …
