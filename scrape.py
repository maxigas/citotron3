#!/usr/bin/python3
# Wikipedia citation tools
# scrape.py
# Scraper

__version__ = '3.0.0'

from functools import lru_cache
from itertools import repeat
from joblib import Parallel, delayed
from multiprocessing.dummy import Pool
from time import sleep
import csv, json, sys, re, os

from args import args
from resolve import *
import resolve, settings
import utils

logfieldnames = "page_id", "page_title", "rev_id", "timestamp", "type", "id", "title", "title_token", "publisher", "publisher_token", "academic?"

if args.restart:
    restartrows = utils.readlog()
else:
    restartrows = []

# restartrows = utils.readlog() if args.restart else None

def restart(row):
    global restartrows
    for rr in restartrows:
        if rr['id'] == row['id'] and rr['rev_id'] == row['rev_id']:
            restartrows.remove(rr)
            return True
    return False


def preprocess(row):
    """
    Preprocessing tasks:
    0. Fix ISBN
    """
    return utils.isbnfix(row)


def resolverow(row, n=0):
    """
    WARNING: Recursive function!
    Resolves title and publisher of row based on id.
    - Calls resolver function resolve.<kind><n>.
    - Title found is success.
    - Title not found calls back this function with same args except n+1.
    - No resolver found is the exit condition.
    """
    row = preprocess(row)
    if args.restart:
        if restart(row):
            return
    resolver = row['type'] + str(n)
    try:
        resolved = globals()[resolver](row['id'])
        # OMG: The comma is necessary here (to make it a list, not a tuple?):
        row['title'], row['publisher'] = *resolved,
        sleep(args.sleep)
        if row['title']:
            postprocess(row)
        else:
            resolverow(row, n+1)
    except KeyError:
        postprocess(row)


def postprocess(row):
    """
    Postprocessing tasks:
    0. There are already two new columns compared to input: title and publisher.
    1. Add columns: title_token and publisher_token. These are for comparison.
    2. Add column: academic?
    """
    # Check if title, publisher was found and if yes then postprocess it:
    row['title_token'] = utils.expand(utils.canonical(row['title'])) if row['title'] else None
    row['publisher_token'] = utils.canonical(row['publisher']) if row['publisher'] else None
    # Everything except books are automatically considered academic:
    if row['type'] != 'isbn':
        row['academic?'] = True
    # Books with no publisher found are neither academic nor non-academic:
    elif not row['publisher']:
        row['academic?'] = None
    # Books with publishers are academic if:
    # 1. the publisher is in the list of academic publishers OR
    # 2. listed in the Thompson-Reuters Master Book List.
    else:
        row['academic?'] = row['publisher_token'] in academic_publishers() # or resolve.is_academic(row['publisher'])
    with open(settings.logfile, 'a', encoding='utf-8') as f:
        global logfieldnames
        log = csv.DictWriter(f, fieldnames=logfieldnames,
                             delimiter='|',
                             quotechar='"',
                             quoting=csv.QUOTE_MINIMAL)
        log.writerow(row)


def main(*, only=False, but=False):
    """
    Resolve in parallel the selected rows to titles.
    """
    global logfieldnames
    if not args.restart:
        with open(settings.logfile, 'w', encoding='utf-8') as logfile:
            log = csv.DictWriter(logfile,
                                 fieldnames=logfieldnames,
                                 delimiter='|',
                                 quotechar='"',
                                 quoting=csv.QUOTE_MINIMAL)
            log.writeheader()
    with open(os.path.join(settings.datadir, args.inputfile), 'rt') as inputfile:
        rows = csv.DictReader(inputfile, delimiter="\t")
        if only:
            Parallel(n_jobs=args.jobs, verbose=51)(
                delayed(resolverow)(r) for r in rows if r['type'] == only)
        elif but:
            Parallel(n_jobs=args.jobs, verbose=51)(
                delayed(resolverow)(r) for r in rows if r['type'] != but)
        else:
            Parallel(n_jobs=args.jobs, verbose=51)(
                delayed(resolverow)(r) for r in rows)


# ---- MISC. FUNCTIONS ----

@lru_cache(maxsize=1) 
def academic_publishers():
    """
    Return a list of academic publishers read from file.
    This is like a global variable but instead it is a memoized function.
    The advantage is that it only initialises itself if it is necessary.
    You can call that a "lazy static variable".
    maxsize=1 is enough because the function has no arguments.
    """
    with open(os.path.join(settings.datadir, settings.academic_publishers),
            mode='rt', encoding='utf-8') as f:
        publishers = [utils.canonical(l.strip()) for l in f.readlines()]
    abbrevs = {'SOC': ' SOCIETY OF',
               'ACAD': 'ACADEMY OF ',
               'UNIV': 'UNIVERSITY OF',
               'NATL': 'NATIONAL',
               'PUBL': 'PUBLISHING',
               'LTD': 'LIMITED',
               'CORP': 'CORPORATION',
               'INC': 'INCORPORATED',
               'COLL': 'COLLEGE',
               'CO': ' COMPANY',
               'INST': 'INSTITUTE',
               'INT': 'INTERNATIONAL',
               'M I T': 'MIT',
               'MIT': 'MIT',
               'DIV': 'DIVISION',
               'LLC': 'LIMITED',
               'PUBS': 'PUBLISHERS'}
    for n, publisher in enumerate(publishers):
        for abbrev in abbrevs:
            r = re.compile('^abbrev\s|\sabbrev$|\sabbrev\s'.replace('abbrev', abbrev))
            if re.match(r, publisher):
                # print('******')
                # print(abbrev)
                # print(publisher)
                # print('++++++')
                publishers[n] = publisher.replace(abbrev, abbrevs[abbrev])
                # print(publishers[n])
                # print('......')
    # print(publishers)
    publishers.append('LAROUSSE')
    return publishers


def count_titles(titles):
    """
    Accepts a list of titles, logging and returning their frequencies.
    """
    freq = counter(titles).most_common()
    for k, v in freq:
        out.writerow([k, v])
    return freq
