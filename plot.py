#!/usr/bin/python3
# Wikipedia citation tools
# plot.py
# Plotting

import csv
import matplotlib.pyplot as plt
import numpy as np
from args import args as a


def titlepop(disc_ratio=a.discriminant):
    """
    Takes the name of a file with the dataset
    and a discriminant ratio, then shows a plot.
    """

    # Extracting the data
    with open(a.outputfile, 'rt') as f:
        data = list(csv.reader(f, delimiter="|"))
    freq_n = [int(x[1]) for x in data[1:] if int(x[1]) >= disc_ratio]
    fail_n = data[0][1]

    # Needed for drawing
    ind = np.arange(len(freq_n))
    bar_width = 0.50
    fig, ax = plt.subplots()

    # Drawing the bars
    rect = ax.bar(ind, freq_n, width=bar_width, color='b', linewidth=0)

    # Description labels
    plt.title("Top quoted publications")
    plt.ylabel("Occurrences")
    plt.xlabel("Publications")

    # Showing the plot
    plt.show()



