#!/usr/bin/python3
# Wikipedia citation tools
# citotron.py
# Main file

import scrape, report, analyse, plot
from args import args

# ---- CALLS ----

mode = args.mode
if mode == "count":
    scrape.main()
    report.failrate()
    analyse.titlepop()
    plot.titlepop()
elif mode == "resolve":
    scrape.main()
    report.failrate()
elif mode == "types":
    report.types()
elif mode == "scisbn":
    scrape.main(only='isbn')
    report.failrate()
    report.scisbn()
elif mode == "journalpop":
    scrape.main(but='isbn')
    report.failrate()
    analyse.titlepop()
    report.mostpop(12)
    plot.titlepop()
elif mode == "bookpop":
    scrape.main(only='isbn')
    report.failrate()
    analyse.titlepop()
    report.mostpop(12)
    plot.titlepop()
else:
    print("mode was: '" + str(mode) + "'")
    print("="*80)
    print("You have to choose a supported mode!")
    print("Try to run the script without --help to get a help message.")




