#!/usr/bin/python3
# Wikipedia citation tools
# analyse.py
# Data analysis

from utils import readlog
from collections import Counter as counter
from args import args as a
import csv

def titlepop():
    titles = [r['title_token'] for r in readlog()]
    freq = counter(titles).most_common()
    with open(a.outputfile, 'w', encoding='utf-8') as f:
        out = csv.writer(f,
                         delimiter='|',
                         quotechar='"',
                         quoting=csv.QUOTE_MINIMAL)
        for k, v in freq:
            out.writerow([k, v])
