# Wikipedia citation tools
# utils.py
# Utilities

from args import args
from json import dump, load
from requests import get, post
import latexcodec, re, ftputil, urllib.request, tarfile, os, csv
import settings as s
from functools import lru_cache
from pprint import pprint


def percent(fraction, total, precision=4):
    return str(fraction/float(total)*100)[:precision]+"%"


def filter_kind(kind, rows):
    return [row for row in rows if row['type'] == kind]


def readdata():
    with open(os.path.join(s.datadir, args.inputfile), 'rt', encoding='utf-8') as f:
        log = csv.DictReader(f, delimiter='\t')
        rows = list(log)
    return rows


def readlog():
    with open(s.logfile, 'r', encoding='utf-8') as f:
        log = csv.DictReader(f,
                             delimiter='|',
                             quotechar='"',
                             quoting=csv.QUOTE_MINIMAL)
        rows = list(log)
        for row in rows:
            newrow = {}
            for k,v in row.items():
                if v == 'True':
                    newrow[k] = True
                elif v == 'False':
                    newrow[k] = False
                elif v == '':
                    newrow[k] = None
                else:
                    newrow[k] = v
            rows.remove(row)
            rows.append(newrow)
    return rows


def readoutput():
    with open(args.outputfile, 'rt') as f:
        rows = list(csv.reader(f, delimiter="|"))
    return rows

def safeget(url):
    try:
        return get(url, timeout=args.timeout)
    except:
        raise


def safepost(url, data):
    try:
        return post(url, timeout=args.timeout, data=data)
    except:
        raise


def _make_journal_abbrevs():
    '''
    Generate ISO 4 List of Title Word Abbreviations (LTWA).
    http://www.issn.org/services/online-services/access-to-the-ltwa/
    Based on the files published with this project:
    http://www.compholio.com/latex/jabbrv/
    Output file:
    1. journal_abbrevs.json - Journal Title Abbreviations
    2. journal_partial_abbrevs.json - Partial Journal Title Abbreviations -- NOT USED!
    '''
    inputfilenames = [
        "data/jabbrv/jabbrv-ltwa-all.ldf",
        "data/jabbrv/jabbrv-ltwa-de.ldf",
        "data/jabbrv/jabbrv-ltwa-en.ldf",
        "data/jabbrv/jabbrv-ltwa-es.ldf",
        "data/jabbrv/jabbrv-ltwa-fr.ldf",
    ]
    lines, journal_abbrevs = [], []
    for inputfilename in inputfilenames:
        with open(inputfilename, mode='rb') as f:
            lines += [l.decode('latex').strip() for l in f.readlines()]
    for l in lines:
        if ("%%" in l) or not l:
            continue
        parts = [p.rstrip("}") for p in l.split("{")]
        if "JournalAbbreviation" in parts[0]:
            journal_abbrevs.append((parts[2], parts[1]))
        elif "JournalPartialAbbreviation" in parts[0]:
            pass
    journal_abbrevs = dict(journal_abbrevs)
    with open('data/journal_abbrevs.json', mode='w', encoding='utf-8') as f:
        dump(journal_abbrevs, f, indent=0, ensure_ascii=False)
    with open('data/journal_partial_abbrevs.json', mode='w', encoding='utf-8') as f:
        dump(journal_partial_abbrevs, f, indent=0, ensure_ascii=False)
    print("Output saved in data/ to 'journal_abbrevs.json' and 'journal_partial_abbrevs.json'.")


def _make_pmid_journal_abbrevs():
    '''
    Make a JSON dictionary from J_Entrez.txt file downloaded from here:
    ftp://ftp.ncbi.nih.gov/pubmed/J_Entrez.txt
    * The dictionary only has the abbreviations.
    * Key is the abbreviation and value is the expanded form.
    '''
    with open("data/J_Entrez.txt", 'rt', encoding='ascii') as f:
        lines = f.readlines()
    shortforms = [l.split(':')[1].strip() for l in lines if 'MedAbbr' in l]
    longforms = [l.split(':')[1].strip() for l in lines if 'JournalTitle' in l]
    abbrevs = {k:v for k,v in zip(shortforms,longforms)}
    with open("data/pmid_journal_abbrevs.json", 'w', encoding='utf-8') as f:
        dump(abbrevs, f, indent=0, ensure_ascii=False)
    print("Output is in data/pmid_journal_abbrevs.json")


def _get_abbrev_files():
    print('Downloading text file from ftp.ncbi.nih.gov')
    with ftputil.FTPHost('ftp.ncbi.nih.gov', 'anonymous', 'citotron@example.com') as h:
	    host.download('pubmed/J_Entrez.txt', 'data/J_Entrez.txt')
    print('Getting tar file from')
    print('http://www.compholio.com/latex/jabbrv/downloads/jabbrv_2014-01-21.tar.gz')
    urllib.request.urlretrieve('http://www.compholio.com/latex/jabbrv/downloads/jabbrv_2014-01-21.tar.gz', 'data/jabbrv_2014-01-21.tar.gz')
    with tarfile.open("data/jabbrv_2014-01-21.tar.gz") as t:
        t.extractall(path='data/')
    _make_journal_abbrevs()
    _make_pmid_journal_abbrevs()


def abbrevs(filename):
    filename = os.path.join(s.datadir, filename)
    if not os.path.isfile(filename): _get_abbrev_files()
    with open(filename, mode='rt', encoding='utf-8') as f:
        return {canonical(k):canonical(v) for k,v in load(f).items()}


def is_valid_isbn(isbn):
    """Takes an ISBN string and return True if it's a valid ISBN"""
    # Best way to test for in bash is:
    # grep isbn cites.csv | awk --field-separator '\t' '{print $6}' | grep -P $REGEX
    # Laxer:
    # regex = re.compile("^(\d{9})(\d|X)$|^(\d{13})$")
    # Stricter:
    regex = re.compile("^(\d{9})(\d|X)$|^(978|979)(\d{10})$")

    if not regex.search(isbn):
        return False
        
    # Split into a list
    chars = list(isbn)
    # Remove the final ISBN digit from `chars`, and assign it to `last`
    last = chars.pop()

    if len(chars) == 9:
        # Compute the ISBN-10 check digit
        val = sum((x + 2) * int(y) for x,y in enumerate(reversed(chars)))
        check = 11 - (val % 11)
        if check == 10:
            check = "X"
        elif check == 11:
            check = "0"

    else:
        # Compute the ISBN-13 check digit
        val = sum((x % 2 * 2 + 1) * int(y) for x,y in enumerate(chars))
        check = 10 - (val % 10)
        if check == 10:
            check = "0"

    return (str(check) == last)


def isbnfix(row):
    """Some ISBNs end with small x, which is against the ISBN specification"""
    if row['type'] == 'isbn' and 'x' in row['id']:
        row['id'] = row['id'].replace('x', 'X')
    return row


def canonical(string):
    """Bring string to canonical form"""
    return re.sub("[^a-zA-z0-9]+", " ", string).strip().upper()


journal_abbrevs = abbrevs(s.journal_abbrevs)
pmid_journal_abbrevs = abbrevs(s.pmid_journal_abbrevs)


def expand(title):
    """Expand abbreviations in title."""
    if title in journal_abbrevs.keys():
        return journal_abbrevs[title].strip()
    if title in pmid_journal_abbrevs.keys():
        return  pmid_journal_abbrevs[title].strip()
    return title
