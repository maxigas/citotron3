# Wikipedia citation tools
# args.py
# Parse command line arguments to args.args object attributes

from argparse import ArgumentParser
from pprint import pprint
import os, sys

parser = ArgumentParser()
parser.add_argument("-m", "--mode", dest="mode", default="count",
                    help="Action to perform on data. 'count' counts the frequency of publication (book, journal, etc.) titles in the dataset. 'resolve' only resolves identifiers to titles (used for testing). 'types' counts the frequency of the identifiers themselves in the dataset (used for understanding the dataset). 'scisbn' counts the ratio of academic vs. non-academic references. 'journaltitles writes a list of journal titles excluding books (the latter is identified by ISBN.'")
parser.add_argument("-k", "--kind", dest="kind", default="all",
                    help="Kind of identifier to process. Default is 'all'. Current choices are 'isbn', 'pmid', 'pmd', 'doi', 'arxiv'.")

parser.add_argument("-i", "--inputfile", dest="inputfile", default="input.csv",
                    help="Input filename. Default is 'input.csv'.")

parser.add_argument("-r", "--restart", dest="restart", default=False, type=bool,
                    help="Restart scraping. Allowed values are 'True' and 'False'. Default is False.")

parser.add_argument("-c", "--comparefile", dest="comparefile", default="other.csv",
                    help="Compare with filename. Default is 'other.csv'.")

parser.add_argument("-o", "--outputfile", dest="outputfile", default="output.csv",
                    help="Output filename. Default is 'output.csv'.")

parser.add_argument("-d", "--discriminant", dest="discriminant", default=10, type=int,
                    help="Plotting parameter. Exclude results when frequency is lower than this constant. Use in combination with 'count' mode. Default is 10.")
    
parser.add_argument("-s", "--sleep", dest="sleep", default=0, type=int,
                    help="Sleep between requests. Default is 0.")

parser.add_argument("-t", "--timeout", dest="timeout", default=6, type=int,
                    help="Time to wait for response from server. Default is 6.")

parser.add_argument("-j", "--jobs", dest="jobs", default=1, type=int,
                    help="Number of parallel requests. Default is 1.")

parser.add_argument("-v", "--verbose", dest="debug", action="store_true",
                    help="Verbose mode. Default is True.")

# Only import this name into other modules of citotron!
args = parser.parse_args()

# Set shell output buffering to 1 by reopeining stdout
sys.stdout = os.fdopen(sys.stdout.fileno(), 'w', 1)
