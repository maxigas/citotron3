# Citotron settings

datadir = "data"
logfile = "log.csv"
academic_publishers = "scientific_publishers.lst"
journal_abbrevs = "journal_abbrevs.json"
partial_journal_abbrevs = "journal_partial_abbrevs.json"
pmid_journal_abbrevs = "pmid_journal_abbrevs.json"
